<?php

/**
 * Created by PhpStorm.
 * User: sumanta
 * Date: 7/22/2017
 * Time: 4:21 PM
 */
class Priority extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function getPriorities(){
        $res =  $this->db->get('priorities')->result();
        return $res;
    }
}