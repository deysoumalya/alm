<?php

/**
 * Created by PhpStorm.
 * User: sumanta
 * Date: 7/22/2017
 * Time: 9:02 PM
 */
class Users extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function getAll(){
        return $this->db->get('users')->result();
    }
}