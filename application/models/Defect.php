<?php

/**
 * Created by PhpStorm.
 * User: sumanta
 * Date: 7/22/2017
 * Time: 5:01 PM
 */
class Defect extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function createDefect(){
        $error=$this->input->post('error');
        $name=$this->input->post('name');
        $stbno=$this->input->post('stbno');
        $vcno=$this->input->post('vcno');
        $address=$this->input->post('address');
        $phno=$this->input->post('phno');
        $aphno=$this->input->post('aphno');
        $status=$this->input->post('status');
        $last_updated_by=$this->input->post('last_updated_by');
        $details=$this->input->post('details');
        $assigned_to=$this->input->post('assigned_to');
        $assigned_no=$this->input->post('assigned_no');
        $this->db->insert('defects', array(
            "error"=>$error,
            "name"=>$name,
            "stbno"=>$stbno,
            "vcno"=>$vcno,
            "address"=>$address,
            "phno"=>$phno,
            "aphno"=>$aphno,
            "status"=>"Pending",
            "last_updated_by"=>date('Y-m-d H:i:s'),
            "details"=>$details,
            "assigned_to"=>$assigned_to,
            "assigned_no"=>$assigned_no,
            
        ));
        $comment=array(
            "defect_id"=>$this->db->insert_id(),
            "owner_id"=>$this->session->userdata('userid'),
            "content"=>"User ".$this->session->userdata('username')." created the defect on ".date('Y-m-d H:i:s')
        );
        $this->db->insert('comments', $comment);
    }
    public function updateDefect($id){
        $status=$this->input->post('status');
        $details=$this->input->post('details');
        $assigned_to=$this->input->post('assigned_to');
        $assigned_no=$this->input->post('assigned_no');
        $this->db->where('id', $id);
        $update=array(
            "status"=>$status,
            "last_updated_by"=>date('Y-m-d H:i:s'),
            "assigned_to"=>$assigned_to,
            "assigned_no"=>$assigned_no                       
        );
        $this->db->update('defects', $update);
        $comment=array(
            "defect_id"=>$id,
            "owner_id"=>$this->session->userdata('userid'),
            "content"=>"User ".$this->session->userdata('username')." updated the defect on ".date('Y-m-d H:i:s')
        );
        $this->db->insert('comments', $comment);
    }

    public function getAll(){
        $this->db->select('defects.id, defects.error, defects.name, defects.stbno, defects.vcno, defects.address, defects.phno, defects.aphno, defects.status, defects.last_updated_by, defects.details, defects.assigned_to, defects.assigned_no ');
        //$this->db->join('priorities','defects.priority = priorities.priority');
        //$this->db->join('users','defects.raised_by = users.id');
        $this->db->order_by('defects.last_updated_by desc');
        $defects = $this->db->get('defects')->result();
        foreach ($defects as $defect){
            $this->db->where('error', $defect->error);
            //$this->db->select('comments.created_at, comments.content, users.user_name as comment_owner');
            //$this->db->join('users', 'comments.owner_id=users.id');
            //$defect->comments = $this->db->get('comments')->result();
        }
        return $defects;
    }

    /* Receive a single record from defects table where the defect id is given to you */

    public function getDefectById($which_id){
        $this->db->select('defects.id, defects.error, defects.name, defects.stbno, defects.vcno, defects.address, defects.phno, defects.aphno, defects.status, defects.last_updated_by, defects.details, defects.assigned_to, defects.assigned_no');
        $this->db->where('defects.id', $which_id);
        $defect = $this->db->get('defects')->row();
        return $defect;
        //return $this->db->query("select * from defects where id = " .  $which_id)->row();
    }

    // function getTimestamp(){
    //     $date = date_create(date('Y-m-d', timezone_open('Asia/Kolkata'));
    //     return date_format($date, 'Y-m-d H:i:sP');
    // }

    public function sendMessage($phoneNumber, $message){
        /*Send SMS using PHP*/    
        
        //Your authentication key
        $authKey = "152755ATVYIOo6E591c3270";
        
        //Multiple mobiles numbers separated by comma
        $mobileNumber = $phoneNumber;
        
        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "QBXGCN";
        
        //Your message to send, Add URL encoding here.
        $message = urlencode($message);
        
        //Define route 
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        
        //API URL
        $url="https://control.msg91.com/api/sendhttp.php";
        
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));
        

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        
        //get response
        $output = curl_exec($ch);
        
        
        curl_close($ch);
    }
}