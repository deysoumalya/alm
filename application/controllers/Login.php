<?php

/**
 * Created by PhpStorm.
 * User: sumanta
 * Date: 7/22/2017
 * Time: 6:02 PM
 */
class Login extends CI_Controller
{
    public function index(){
        $data=array();
        if($this->session->userdata('loggedin')){
            redirect('Dashboard');
        }
        if($this->input->method()=="post"){
            $username=$this->input->post("username");
            $password=$this->input->post("password");
            $this->db->where('user_name',$username);
            $this->db->where('password',$password);
            $user=$this->db->get('users')->row();
            if($user!=null){
                $this->session->set_userdata('loggedin',true);
                $this->session->set_userdata('username', $user->user_name);
                $this->session->set_userdata('userid', $user->id);
                $this->session->set_userdata('userrole', $user->role);
                redirect('Dashboard');
            }else{
                $data['message']='Wrong username/password';
            }
        }
        $this->load->view('login', $data);
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('Login');
    }
}