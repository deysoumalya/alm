<?php

/**
 * Created by PhpStorm.
 * User: sumanta
 * Date: 7/22/2017
 * Time: 3:24 PM
 */
class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('loggedin')){
            redirect('Login');
        }
    }


    public function index(){    
        $data=array();
        $data['priorities']=$this->Priority->getPriorities();
        $data['users']=$this->Users->getAll();
        if($this->input->method()=="post"){
            if($this->input->post('update')=="0") {
                $this->Defect->createDefect();
                $this->Defect->sendMessage($defect->phno, 'Your complaint is in pending status');
               }
                else if($this->input->post('update')=="1"){
                $this->Defect->updateDefect($this->input->post('id'));
                $status= $this->input->post('status');
                $defect= $this->Defect->getDefectById($this->input->post('id'));
                if ($status == 'Pending') {
                    $this->Defect->sendMessage($defect->phno, 'Your complaint is in pending status');
                }else if ($status == 'Accept'){
                    $this->Defect->sendMessage($defect->phno, 'Your complaint has been accepted');
                    $this->Defect->sendMessage($defect->assigned_no, 'Complaint Id: '.$defect->id.' For user '.$defect->name.' has been accepted.');
                }else if ($status == 'Resolved'){
                    $this->Defect->sendMessage($defect->phno, 'Your complaint has been resolved successfully!');
                }
            }
        }
        $data['defects']=$this->Defect->getAll();
        $this->load->view('dashboard', $data);
    }

    
}



?>