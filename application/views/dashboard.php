<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Qbex Cable Complaint Management</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url() ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url() ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url() ?>bower_components/Ionicons/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="<?= base_url() ?>dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .modal {
            text-align: center;
            padding: 0!important;
        }

        .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
        }

        .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
        }
    </style>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Qbex Cable</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="<?= base_url() ?>user-xxl.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?= $this->session->userdata('username') ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?= base_url() ?>index.php/Login/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= base_url() ?>user-xxl.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?= $this->session->userdata('username') ?></p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <!-- Optionally, you can add icons to the links -->
                <li class="active"><a href="#"><i class="fa fa-link"></i> <span>All Complaints</span></a></li>
                <li  <?= ($this->session->userdata('userrole') == 'lmo')?'hidden="hidden"':'' ?>><a href="#" data-toggle="modal" data-target="#myModal" ><i class="fa fa-link" ></i> <span>New Complaints</span></a></li>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Complaints
                <small>List of complaints</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
                <li class="active">Home</li>
            </ol-->
        </section>

        <!-- Main content -->
        <section class="content container-fluid">

            <div class="row pull-right">
                <div class="col-md-4">
                    <!--button class="btn btn-primary btn-sm"  data-toggle="#" data-target="#"  <?= ($this->session->userdata('userrole') == 'admin')?'enabled="enabled"':'' ?>>Send SMS</button-->
                </div>
            </div>
            <br/>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-12">
                    <table id="defects-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Error Code</th>
                            <th>Name</th>
                            <th>STB Number</th>
                            <th>V.C Number</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                            <th>Alternate Phone Number</th>
                            <th>Status</th>
                            <th>Last Updated on</th>
                            <th>Details</th>
                            <th>Assigned To</th>
                            <th>Assigned Employee Number</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $count=1;foreach($defects as $defect) {?>
                                <tr>
                                    <!--td></td>
                                    <td></a></td>
                                    <td><a href="#" onclick="openUpdateDialog(<?=$count-2?>)"><?= $defect->title ?></a></td>
                                    <td><?= $defect->assigned_to ?></td>
                                    <td><?= $defect->raised_by_name ?></td>
                                    <td><?= $defect->created_at ?></td>
                                    <td><a href="#" onclick="openUpdateDialog(<?=$count-2?>)"><?= $defect->status ?></a></td>
                                    <td></td>
                                    <td><?= $defect->priority_detail ?></td-->
                                    <td><?= $count++ ?></td>
                                    <td><?= $defect->error ?></td>
                                    <td><?= $defect->name ?></td>
                                    <td><?= $defect->stbno ?></td>
                                    <td><?= $defect->vcno ?></td>
                                    <td><?= $defect->address ?></td>
                                    <td><?= $defect->phno ?></td>
                                    <td><?= $defect->aphno ?></td>
                                    <td><a href="#" onclick="openUpdateDialog(<?=$count-2?>)"><?= $defect->status ?></a></td>
                                    <td>
                                        <script>document.write((new Date('<?= $defect->last_updated_by ?> +0200')).toLocaleString())</script>
                                    </td>
                                    <td><?= $defect->details ?></td>
                                    <td><?= $defect->assigned_to ?></td>
                                    <td><?= $defect->assigned_no ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <section id="modal">
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" <?= ($this->session->userdata('userrole') == 'lmo')?'hidden="hidden"':'' ?>>&times;</button>
                            <h4 class="modal-title">New Complaint</h4>
                        </div>
                        <form id="form" role="form" method="post" action="<?= base_url() ?>index.php/Dashboard" enctype="multipart/form-data">
                            <div class="modal-body">
                                    
                                    <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">Error Code</label>
                                        <input type="text" class="form-control" name="error" id="error">
                                    </div>

                                    <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">Name</label>
                                        <input type="text" class="form-control" name="name" id="name">
                                    </div>

                                    <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">STB Number</label>
                                        <input type="text" class="form-control" name="stbno" id="stbno">
                                    </div>
                                    <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">V.C Number</label>
                                        <input type="number" class="form-control" name="vcno" id="vcno">
                                   </div>
                               <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">Address</label>
                                        <input type="text" class="form-control" name="address" id="address">
                                   </div>
                                 
                               <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">Phone Number</label>
                                        <input type="text" class="form-control" name="phno" id="phno">
                                   </div>
                              <input name="update" value="0" hidden="hidden">
                                    <div class="form-group">
                                        <label for="title">Alternate Phone Number</label>
                                        <input type="text" class="form-control" name="aphno" id="aphno">
                                    </div>        
                                    <!--div class="form-group">
                                        <label>Priority</label>
                                        <select class="form-control" name="priority" id="priority">
                                            <?php foreach ($priorities as $priority){ ?>
                                                <option value="<?= $priority->priority ?>"><?= 'P'.$priority->priority.' ('.$priority->description.')' ?></option>
                                            <?php } ?>
                                        </select>
                                    </div-->
                                   
                                    <!--div class="form-group">
                                        <label>Assigned To</label>
                                        <input class="form-control" name="assigned_to" id="assigned_to" <?= ($this->session->userdata('userrole') == 'admin')?'disabled="disabled"':'' ?>>
                                                <option value="<?= $defect->assigned_to ?>"></option>
                                    </div-->
                                    
                                    <!--div class="form-group">
                                        <label for="file">Attachment</label>
                                        <input type="file" class="form-control" name="file" id="file">
                                    </div-->
                                    
                                    <div class="form-group">
                                        <label for="details">Details</label>
                                        <textarea class="form-control" rows="3" id="details" name="details">
                                            
                                        </textarea>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="submitForm()">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </section>
        <!--Update Part-->
        <section id="update-modal-section">
            <!-- Modal -->
            <div id="update-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Status</h4>
                        </div>
                        <form id="form2" role="form" method="post" action="<?= base_url() ?>index.php/Dashboard" enctype="multipart/form-data">
                            <div class="modal-body">
                                <input name="update" value="1" hidden="hidden">
                                <input name="id" id="id" hidden="hidden">
                                <!--div class="form-group">
                                    <label for="utitle">Title</label>
                                    <input type="text" class="form-control" name="title" id="utitle">
                                </div-->
                                <div class="form-group">
                                    <label for="utitle">Assigned To</label>
                                    <input name= "assigned_to" type="text" enabled="enabled" class="form-control" id="newassigned_to" <?= ($this->session->userdata('userrole') == 'admin')?'disabled="disabled"':'' ?>>
                                </div>

                                <div class="form-group">
                                    <label for="utitle">Assigned Employee Number</label>
                                    <input name= "assigned_no" type="text" enabled="enabled" class="form-control" id="newassigned_no" <?= ($this->session->userdata('userrole') == 'admin')?'disabled="disabled"':'' ?>>
                                </div>
                                
                                <!--div class="form-group">
                                        <label for='utitle'>Assign To</label>
                                        <input name= "assigned_to" type="text" enabled="enabled" class="form-control" id="newassigned_to" <?= ($this->session->userdata('userrole') == 'admin')?'disabled="disabled"':'' ?>>
                                            <option value="<?= $defect->assigned_to ?>"></option>
                                        </select>
                                </div--> 
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" id="newstatus" <?= ($this->session->userdata('userrole') == 'admin')?'disabled="disabled"':'' ?>>
                                        <option value="Pending">Pending</option>
                                        <option value="Accept">Accept</option>
                                        <option value="Resolved">Resolved</option>
                                        <option value="Unresolved">Unresolved</option>
                                    </select>
                                </div>
                                <!--div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control" name="priority" id="upriority">
                                        <?php foreach ($priorities as $priority){ ?>
                                            <option value="<?= $priority->priority ?>"><?= 'P'.$priority->priority.' ('.$priority->description.')' ?></option>
                                        <?php } ?>
                                    </select>
                                </div-->
                                <!--div class="form-group">
                                    <label for="file">Attachment</label>
                                    <input type="file" class="form-control" name="file" id="file">
                                </div-->
                                <!--div class="form-group">
                                    <label for="details">Details</label>
                                    <textarea class="form-control" rows="3" id="details" name="details"></textarea>
                                </div-->
                                <!--div class="form-group">
                                    <label for="details">Details</label>
                                    <div class="well" style="height: 150px; overflow-y: auto">
                                        <ul id="comments">

                                        </ul>
                                    </div>
                                </div-->
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="submitUpdateForm()">Update</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">

        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; <?= date('Y') ?> <a href="mailto:business@qbexsolution.com">Qbex Cable</a>.</strong> IP.
    </footer>

    <!-- Control Sidebar -->
    <!--aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <!--ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <!--div class="tab-content">
            <!-- Home tab content -->
            <!--div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <!--h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <!--div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <!--div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <!--div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?= base_url() ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dist/js/adminlte.min.js"></script>
<script>
    $(function () {
        $('#defects-table').DataTable()
    })
    function submitForm(){
        //$("#form").submit();
        $.post("<?= base_url() ?>index.php/Dashboard", $("#form").serialize(), function(data){
            location.reload();
        })
    }
    function submitUpdateForm(){
        $.post("<?= base_url() ?>index.php/Dashboard", $("#form2").serialize(), function(data){
            location.reload();
        })
    }
    function openUpdateDialog(error){
        var defect=defects[error];
        console.log(defect);
        $("#id").val(defect.id);/*
        $("#name").val(defect.name);
        $("#stbno").val(defect.stbno);
        $("#vcno").val(defect.vcno);
        $("#address").val(defect.address);
        $("#phno").val(defect.phno);
        $("#aphno").val(defect.aphno);*/
        $("#newstatus").val(defect.status).change();
        $("#newassigned_to").val(defect.assigned_to).change();
        $("#newassigned_no").val(defect.assigned_no).change();
        if(defect.comments){
            defect.comments.map(function(comment){
                $("#comments").empty();
                $("#comments")
                    .append('<li><b><font color="blue">'+comment.comment_owner+'</font></b> ('+comment.created_at+'): '+comment.content+'</li>');
            });
        }
        $("#update-modal").modal('show');
    }
    var defects=<?= json_encode($defects) ?>;
    console.log(defects);
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>