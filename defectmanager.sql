-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2017 at 05:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `defectmanager`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `getUserNameFromId` (`uid` INT) RETURNS VARCHAR(80) CHARSET utf8 BEGIN
	DECLARE displayname varchar(80);
		select user_name into displayname from users where id=uid;
	RETURN displayname;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `defect_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `content` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `defect_id`, `owner_id`, `content`, `created_at`) VALUES
(109, 1, 7, 'User Soumalya.Dey created the defect on 2017-09-09 18:08:50', '2017-09-09 16:08:50'),
(110, 2, 7, 'User Soumalya.Dey created the defect on 2017-09-10 04:05:40', '2017-09-10 02:05:40'),
(111, 1, 0, 'Defect status has been updated to Resolved', '2017-09-10 15:10:50'),
(112, 0, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:12:07', '2017-09-10 15:12:07'),
(113, 1, 0, 'Defect status has been updated to Pending', '2017-09-10 15:15:22'),
(114, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:15:22', '2017-09-10 15:15:22'),
(115, 1, 0, 'Defect status has been updated to Unresolved', '2017-09-10 15:15:33'),
(116, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:15:33', '2017-09-10 15:15:33'),
(117, 1, 0, 'Defect status has been updated to Accept', '2017-09-10 15:15:39'),
(118, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:15:39', '2017-09-10 15:15:39'),
(119, 1, 0, 'Defect status has been updated to Unresolved', '2017-09-10 15:20:20'),
(120, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:20:20', '2017-09-10 15:20:20'),
(121, 1, 0, 'Defect status has been updated to Pending', '2017-09-10 15:29:59'),
(122, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:29:59', '2017-09-10 15:29:59'),
(123, 1, 0, 'Defect status has been updated to Accept', '2017-09-10 15:30:11'),
(124, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:30:11', '2017-09-10 15:30:11'),
(125, 1, 0, 'Defect status has been updated to Resolved', '2017-09-10 15:32:29'),
(126, 1, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 17:32:29', '2017-09-10 15:32:29'),
(127, 1, 0, 'Defect status has been updated to Pending', '2017-09-10 15:32:55'),
(128, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:32:55', '2017-09-10 15:32:55'),
(129, 1, 0, 'Defect status has been updated to Accept', '2017-09-10 15:33:40'),
(130, 1, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 17:33:40', '2017-09-10 15:33:40'),
(131, 1, 0, 'Defect status has been updated to Unresolved', '2017-09-10 15:34:08'),
(132, 1, 7, 'User Soumalya.Dey updated the defect on 2017-09-10 17:34:08', '2017-09-10 15:34:08'),
(133, 1, 0, 'Defect status has been updated to Accept', '2017-09-10 15:34:45'),
(134, 1, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 17:34:45', '2017-09-10 15:34:45'),
(135, 1, 0, 'Defect status has been updated to Resolved', '2017-09-10 16:00:32'),
(136, 1, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 18:00:32', '2017-09-10 16:00:32'),
(137, 1, 0, 'Defect status has been updated to Accept', '2017-09-10 16:06:53'),
(138, 1, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 18:06:53', '2017-09-10 16:06:53'),
(139, 2, 7, 'User Soumalya.Dey created the defect on 2017-09-10 18:49:48', '2017-09-10 16:49:48'),
(140, 2, 0, 'Defect status has been updated to Resolved', '2017-09-10 16:50:16'),
(141, 2, 8, 'User Sumanta.Banerjee updated the defect on 2017-09-10 18:50:16', '2017-09-10 16:50:16'),
(142, 3, 1, 'User Suranjan.Saha created the defect on 2017-09-11 12:37:36', '2017-09-11 10:37:36'),
(143, 3, 0, 'Defect status has been updated to Accept', '2017-09-11 10:45:49'),
(144, 3, 2, 'User Shubhopam.Deb updated the defect on 2017-09-11 12:45:49', '2017-09-11 10:45:49'),
(145, 4, 1, 'User Suranjan.Saha created the defect on 2017-09-11 13:12:40', '2017-09-11 11:12:40'),
(146, 4, 0, 'Defect status has been updated to Accept', '2017-09-11 11:14:31'),
(147, 4, 2, 'User Shubhopam.Deb updated the defect on 2017-09-11 13:14:31', '2017-09-11 11:14:31'),
(148, 5, 1, 'User Suranjan.Saha created the defect on 2017-09-11 13:15:29', '2017-09-11 11:15:29'),
(149, 6, 1, 'User Suranjan.Saha created the defect on 2017-09-11 13:33:46', '2017-09-11 11:33:46'),
(150, 7, 1, 'User Suranjan.Saha created the defect on 2017-09-11 13:35:11', '2017-09-11 11:35:11'),
(151, 7, 0, 'Defect status has been updated to Resolved', '2017-09-11 11:35:32'),
(152, 7, 2, 'User Shubhopam.Deb updated the defect on 2017-09-11 13:35:32', '2017-09-11 11:35:32'),
(153, 8, 1, 'User Suranjan.Saha created the defect on 2017-09-12 08:04:59', '2017-09-12 06:04:59'),
(154, 8, 0, 'Defect status has been updated to Accept', '2017-09-12 06:05:35'),
(155, 8, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 08:05:35', '2017-09-12 06:05:35'),
(156, 9, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:03:02', '2017-09-12 07:03:02'),
(157, 9, 0, 'Defect status has been updated to Accept', '2017-09-12 07:03:39'),
(158, 9, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:03:39', '2017-09-12 07:03:39'),
(159, 9, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:05:48'),
(160, 9, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:05:48', '2017-09-12 07:05:48'),
(161, 9, 0, 'Defect status has been updated to Unresolved', '2017-09-12 07:06:53'),
(162, 9, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:06:53', '2017-09-12 07:06:53'),
(163, 8, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:08:14'),
(164, 8, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:08:15', '2017-09-12 07:08:15'),
(165, 9, 0, 'Defect status has been updated to Accept', '2017-09-12 07:08:55'),
(166, 9, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:08:55', '2017-09-12 07:08:55'),
(167, 9, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:10:12'),
(168, 9, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:10:12', '2017-09-12 07:10:12'),
(169, 10, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:10:32', '2017-09-12 07:10:32'),
(170, 10, 0, 'Defect status has been updated to Accept', '2017-09-12 07:19:38'),
(171, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:19:38', '2017-09-12 07:19:38'),
(172, 10, 0, 'Defect status has been updated to Pending', '2017-09-12 07:22:14'),
(173, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:22:14', '2017-09-12 07:22:14'),
(174, 10, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:23:31'),
(175, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:23:31', '2017-09-12 07:23:31'),
(176, 10, 0, 'Defect status has been updated to Accept', '2017-09-12 07:25:07'),
(177, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:25:07', '2017-09-12 07:25:07'),
(178, 10, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:26:02'),
(179, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:26:02', '2017-09-12 07:26:02'),
(180, 10, 0, 'Defect status has been updated to Accept', '2017-09-12 07:31:08'),
(181, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:31:08', '2017-09-12 07:31:08'),
(182, 10, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:31:38'),
(183, 10, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:31:38', '2017-09-12 07:31:38'),
(184, 11, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:31:57', '2017-09-12 07:31:57'),
(185, 12, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:32:59', '2017-09-12 07:32:59'),
(186, 12, 0, 'Defect status has been updated to Accept', '2017-09-12 07:33:16'),
(187, 12, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:33:16', '2017-09-12 07:33:16'),
(188, 12, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:46:14'),
(189, 12, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:46:14', '2017-09-12 07:46:14'),
(190, 13, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:46:41', '2017-09-12 07:46:41'),
(191, 14, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:47:17', '2017-09-12 07:47:17'),
(192, 14, 0, 'Defect status has been updated to Accept', '2017-09-12 07:48:48'),
(193, 14, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:48:49', '2017-09-12 07:48:49'),
(194, 13, 0, 'Defect status has been updated to Unresolved', '2017-09-12 07:50:56'),
(195, 13, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:50:56', '2017-09-12 07:50:56'),
(196, 13, 0, 'Defect status has been updated to Accept', '2017-09-12 07:52:20'),
(197, 13, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:52:20', '2017-09-12 07:52:20'),
(198, 15, 1, 'User Suranjan.Saha created the defect on 2017-09-12 09:52:45', '2017-09-12 07:52:45'),
(199, 15, 0, 'Defect status has been updated to Resolved', '2017-09-12 07:53:15'),
(200, 15, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 09:53:15', '2017-09-12 07:53:15'),
(201, 16, 1, 'User Suranjan.Saha created the defect on 2017-09-12 10:00:00', '2017-09-12 08:00:00'),
(202, 16, 0, 'Defect status has been updated to Accept', '2017-09-12 08:00:19'),
(203, 16, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 10:00:19', '2017-09-12 08:00:19'),
(204, 16, 0, 'Defect status has been updated to Resolved', '2017-09-12 08:44:09'),
(205, 16, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 10:44:09', '2017-09-12 08:44:09'),
(206, 17, 1, 'User Suranjan.Saha created the defect on 2017-09-12 11:10:53', '2017-09-12 09:10:53'),
(207, 17, 0, 'Defect status has been updated to Unresolved', '2017-09-12 09:11:15'),
(208, 17, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 11:11:15', '2017-09-12 09:11:15'),
(209, 16, 0, 'Defect status has been updated to Accept', '2017-09-12 09:22:43'),
(210, 16, 2, 'User Shubhopam.Deb updated the defect on 2017-09-12 11:22:43', '2017-09-12 09:22:43'),
(211, 18, 1, 'User Soumalya.Dey created the defect on 2017-09-12 14:25:08', '2017-09-12 12:25:08'),
(212, 18, 0, 'Defect status has been updated to Accept', '2017-09-12 12:25:39'),
(213, 18, 2, 'User Rina.Dey updated the defect on 2017-09-12 14:25:39', '2017-09-12 12:25:39'),
(214, 18, 0, 'Defect status has been updated to Resolved', '2017-09-14 01:15:24'),
(215, 18, 2, 'User Rina.Dey updated the defect on 2017-09-14 03:15:24', '2017-09-14 01:15:24'),
(216, 18, 0, 'Defect status has been updated to Pending', '2017-09-16 14:04:01'),
(217, 18, 2, 'User Rina.Dey updated the defect on 2017-09-16 16:04:02', '2017-09-16 14:04:02'),
(218, 18, 0, 'Defect status has been updated to Accept', '2017-09-16 14:04:21'),
(219, 18, 2, 'User Rina.Dey updated the defect on 2017-09-16 16:04:21', '2017-09-16 14:04:21'),
(220, 18, 0, 'Defect status has been updated to Resolved', '2017-09-16 14:22:09'),
(221, 18, 2, 'User Rina.Dey updated the defect on 2017-09-16 16:22:09', '2017-09-16 14:22:09');

-- --------------------------------------------------------

--
-- Table structure for table `defects`
--

CREATE TABLE `defects` (
  `id` int(11) NOT NULL,
  `error` varchar(250) NOT NULL,
  `name` varchar(80) DEFAULT NULL,
  `stbno` varchar(50) DEFAULT NULL,
  `vcno` varchar(255) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `phno` varchar(50) DEFAULT NULL,
  `aphno` varchar(11) DEFAULT NULL,
  `status` varchar(100) DEFAULT '',
  `last_updated_by` varchar(30) DEFAULT NULL,
  `details` varchar(250) DEFAULT NULL,
  `assigned_to` varchar(50) DEFAULT NULL,
  `assigned_no` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `defects`
--
DELIMITER $$
CREATE TRIGGER `after defect update` AFTER UPDATE ON `defects` FOR EACH ROW BEGIN
	DECLARE message varchar(100);
    SET message = concat('Defect status has been updated to ', NEW.status);
	IF NEW.status <> OLD.status THEN
		insert into comments (defect_id, owner_id, content) values(NEW.id, 0, message);
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `description` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `priority`, `description`) VALUES
(5, 3, 'Minor'),
(6, 2, 'Major'),
(7, 1, 'Show Stopper'),
(8, 0, 'Change Request');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `role` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `defects`
--
ALTER TABLE `defects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT for table `defects`
--
ALTER TABLE `defects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
